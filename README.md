# Mosquitto image

Mosquitto broker image. Based on `eclipse-mosquitto`, with integrated `mosquitto.conf` file


Running image:

        docker run -p 1883:1883 registry.gitlab.com/roxautomation/images/mosquitto:latest
