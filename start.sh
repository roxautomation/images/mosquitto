#!/bin/bash

# start image from gitlab
docker run -d \
	-p 1883:1883 \
	--restart unless-stopped \
	registry.gitlab.com/roxautomation/images/mosquitto:latest
